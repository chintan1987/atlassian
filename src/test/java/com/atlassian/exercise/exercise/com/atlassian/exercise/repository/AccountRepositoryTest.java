package com.atlassian.exercise.exercise.com.atlassian.exercise.repository;

import com.atlassian.exercise.model.Account;
import com.atlassian.exercise.repository.AccountRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void setUp(){
        Account account = new Account();
        account.setCompanyName("Atlassian");
        account.setAddressLine1("1 Market");
        account.setAddressLine2("Street");
        account.setCity("San Fransisco");
        account.setState("CA");
        account.setCountry("USA");
        testEntityManager.persist(account);
    }

    @Test
    public void whenFindAll_thenReturnAllAccount() {
        // when
        List<Account> accounts = accountRepository.findAll();

        // then
        Assert.assertNotNull(accounts);
        Assert.assertEquals(accounts.size(), 1);

    }

}
