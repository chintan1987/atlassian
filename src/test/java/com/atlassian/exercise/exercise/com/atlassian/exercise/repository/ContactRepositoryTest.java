package com.atlassian.exercise.exercise.com.atlassian.exercise.repository;

import com.atlassian.exercise.model.Account;
import com.atlassian.exercise.model.Contact;
import com.atlassian.exercise.repository.AccountRepository;
import com.atlassian.exercise.repository.ContactRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ContactRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private AccountRepository accountRepository;

    private Long accountId = null;

    @Before
    public void setUp(){
        Account account = new Account();
        account.setCompanyName("Atlassian");
        account.setAddressLine1("1 Market");
        account.setAddressLine2("Street");
        account.setCity("San Fransisco");
        account.setState("CA");
        account.setCountry("USA");
        testEntityManager.persist(account);

        accountId = accountRepository.findAll().get(0).getId();

        Contact contact = new Contact();
        contact.setName("Contact1");
        contact.setEmail("abc@xyz.com");
        contact.setAccount(account);
        testEntityManager.persist(contact);

        contact = new Contact();
        contact.setName("Contact2");
        contact.setEmail("def@xyz.com");
        contact.setAccount(account);
        testEntityManager.persist(contact);

        contact = new Contact();
        contact.setName("Contact3");
        contact.setEmail("ghi@xyz.com");
        testEntityManager.persist(contact);
    }

    @Test
    public void whenFindAll_thenReturnAllContact() {
        // when
        List<Contact> contacts = contactRepository.findAll();

        // then
        Assert.assertNotNull(contacts);
        Assert.assertEquals(contacts.size(), 3);
    }

    public void whenFindByAccountId_thenReturnAllContact(){
        // when
        List<Contact> contacts = contactRepository.findByAccountId(accountId);

        // then
        Assert.assertNotNull(contacts);
        Assert.assertEquals(contacts.size(), 2);
    }
}
