package com.atlassian.exercise.controller;

import com.atlassian.exercise.exception.ResourceNotFoundException;
import com.atlassian.exercise.model.Account;
import com.atlassian.exercise.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping("/api/accounts/all")
    public Page<Account> getAllAccounts(Pageable pageable){
        return accountRepository.findAll(pageable);
    }

    @GetMapping("/api/accounts/{accountId}")
    public Account getAccountById(@PathVariable(name = "accountId" )Long accountId){
        Optional<Account> account = accountRepository.findById(accountId);
        return account.get();
    }

    @PostMapping("/api/accounts")
    public Account createAccount(@Valid @RequestBody Account account){
        return accountRepository.save(account);
    }

    @PutMapping("/api/accounts/{accountId}")
    public Account updateContact(@Valid @PathVariable(name = "contactId")Long accountId, @RequestBody Account accountRequest){
        return accountRepository.findById(accountId).map(account -> {
            return accountRepository.save(accountRequest);
        }).orElseThrow(()->new ResourceNotFoundException("AccountId: "+accountId+" not found"));
    }

    @DeleteMapping("/api/accounts/{accountId}")
    public ResponseEntity<?> deleteContact(@PathVariable(name = "accountId") Long accountId){
        return accountRepository.findById(accountId).map(account -> {
            accountRepository.delete(account);
            return ResponseEntity.ok().build();
        }).orElseThrow(()->new ResourceNotFoundException("AccountId: "+accountId+" not found"));
    }
}
