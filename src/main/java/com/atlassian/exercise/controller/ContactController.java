package com.atlassian.exercise.controller;

import com.atlassian.exercise.exception.ResourceNotFoundException;
import com.atlassian.exercise.model.Contact;
import com.atlassian.exercise.repository.AccountRepository;
import com.atlassian.exercise.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ContactController {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping("/api/accounts/{accountId}/contacts")
    public Page<Contact> getAllContactsByAccountId(@PathVariable (value = "accountId") Long accountId,
                                                Pageable pageable) {
        return contactRepository.findByAccountId(accountId, pageable);
    }

    @PostMapping("/api/accounts/{accountId}/contacts")
    public Contact createContactForAccount(@PathVariable (value = "accountId") Long accountId,
                                 @Valid @RequestBody Contact contact) {
        return accountRepository.findById(accountId).map(account -> {
            contact.setAccount(account);
            return contactRepository.save(contact);
        }).orElseThrow(() -> new ResourceNotFoundException("AccountId " + accountId + " not found"));
    }

    @GetMapping("/api/contacts/all")
    public Page<Contact> getAllContacts(Pageable pageable){
        return contactRepository.findAll(pageable);
    }

    @GetMapping("/api/contacts/{contactId}")
    public Contact getContactById(@PathVariable(name = "contactId" )Long contactId){
        Optional<Contact> contact = contactRepository.findById(contactId);
        return contact.get();
    }

    @PostMapping("/api/contacts")
    public Contact createContact(@Valid @RequestBody Contact contact){
        return contactRepository.save(contact);
    }

    @PutMapping("/api/contact/{contactId}")
    public Contact updateContact(@Valid @PathVariable(name = "contactId")Long contactId, @RequestBody Contact contactRequest){
        return contactRepository.findById(contactId).map(contact -> {
            return contactRepository.save(contactRequest);
        }).orElseThrow(()->new ResourceNotFoundException("ContactId: "+contactId+" not found"));
    }

    @DeleteMapping("/api/contacts/{contactId}")
    public ResponseEntity<?> deleteContact(@PathVariable(name = "contactId") Long contactId){
        return contactRepository.findById(contactId).map(contact -> {
            contactRepository.delete(contact);
            return ResponseEntity.ok().build();
        }).orElseThrow(()->new ResourceNotFoundException("ContactId: "+contactId+" not found"));
    }

}
