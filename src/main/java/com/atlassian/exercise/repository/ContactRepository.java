package com.atlassian.exercise.repository;

import com.atlassian.exercise.model.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    Page<Contact> findByAccountId(Long accountId, Pageable pageable);
    List<Contact> findByAccountId(Long accountId);
    Optional<Contact> findByIdAndAccountId(Long id, Long accountId);
}
