Atlassian Code Exercise

Technical Requirements:
- https://bitbucket.org/dcor_atlassian/atlassian-code-exercise-sr-full-stack-dev-martech/src/master/

Project Details:

Technologies Used:
  - Java 8
  - Spring Boot
    - Web
    - JPA
    - H2(File Based DB)
  - Junit
  - maven

Suggested milestones
  - Can the server be started using the documentation?
    - Yes. Open the project, install maven dependencies and run ExerciseApplication
  - Does the application connect to a database?
    - Yes. It used H2 file based DB. This is a temp DB which is not persisted when the server is shutdown
  - Does the account endpoint handle GET/POST/PUT requests?
    - Yes. The account handles all the CRUD Operations.
  - Does the contact endpoint handle GET/POST/PUT requests?
    - Yes. The contact handles all the CRUD operations.
  - Can we associate a contact with an account?
    - Yes. You can associate multiple contacts to a single account
  - Can we get all contacts for an account?
    - Yes. Check out the end point ("/api/accounts/{accountId}/contacts")
  - Have test cases been written?
    - Yes. Have written the repository level tests.

Swagger UI:
  - http://localhost:8080/swagger-ui.html

Rest End Points:

Account:
- POST "/api/accounts" - create an account
- GET "/api/accounts/all" - Returns all accounts
- DELETE "/api/accounts/{accountId}" - Delete an account by id
- GET "/api/accounts/{accountId}" - Get Account by ID
- PUT "/api/accounts/{accountId}" - Update an account

Contact:
- GET "/api/accounts/{accountId}/contacts" - get All Contacts By AccountId
- POST "/api/accounts/{accountId}/contacts" - create Contact For Account
- PUT "/api/contact/{contactId}" - update Contact
- POST "/api/contacts" - create Contact
- GET "/api/contacts/all" - get All Contacts
- DELETE "/api/contacts/{contactId}" delete Contact




